package com.springms.securityissue.controller;

import com.springms.securityissue.model.HelloWorld;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
public class HelloWorldController {

    @RequestMapping(value = "/rapid7", method = RequestMethod.POST)
    public void vulnerable(HelloWorld model) {
        System.out.println("Inside vulnerable() - ");
        if(Objects.nonNull(model)){
            System.out.println("message - " + model.getMessage());
        }
    }

    @RequestMapping(value = "/sayHello", method = RequestMethod.GET)
    public String sayHello() {
        return "sayHello";
    }
}